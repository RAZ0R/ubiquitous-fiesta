# ubiquitous-fiesta

## Pre-requisites

- Install nodejs **v14.5.0**
- Install yarn.
```bash
 $ npm install -g yarn
```
- `cd` into this project directory.
- Install dependencies for this project.
  - **NOTE**: You have to repeat this step each time you take a fresh pull of this repo.
```bash
 $ yarn install
```
- Setup pre-commit hook
  - **NOTE**: This will only modify the git config for this repo.
```bash
 $ git config core.hooksPath hooks
```

## Contributing

### Run locally
- Make sure an emulator or device is connected before running below commands
- For android
```bash
 $ npx react-native run-android
```
- For IOS
```bash
 $ npx react-native run-ios
```

### Linting
- Run below command into this project directory
```bash
 $ yarn lint
```
