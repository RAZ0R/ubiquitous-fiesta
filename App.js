import React, { useEffect } from 'react';
import { createDrawerNavigator } from '@react-navigation/drawer';
import { NavigationContainer } from '@react-navigation/native';
import DrawerContent from './src/screen/DrawerContent';
import {
  HomeStackScreen,
  ItemStackScreen,
  SummonerStackScreen,
  RuneStackScreen,
  CreatureStackScreen,
  SkinStackScreen,
} from './src/screen/StackScreen';

import Style from './src/style/GlobalStyle';
import buildCache from './src/util/CacheManager';
import log from './src/util/Logger';

const Drawer = createDrawerNavigator();

const App = () => {
  useEffect(() => {
    (async () => {
      try {
        await buildCache();
      } catch (err) {
        log.error(err.message);
        log.error(err.stack);
      }
    })();
  }, []);

  return (
    <NavigationContainer>
      <Drawer.Navigator
        drawerContent={(props) => <DrawerContent {...props} />}
        initialRouteName="Build"
        drawerStyle={Style.drawerstyle}
      >
        <Drawer.Screen name="Build" component={HomeStackScreen} />
        <Drawer.Screen name="Item" component={ItemStackScreen} />
        <Drawer.Screen name="Summoner" component={SummonerStackScreen} />
        <Drawer.Screen name="Rune" component={RuneStackScreen} />
        <Drawer.Screen name="Creature" component={CreatureStackScreen} />
        <Drawer.Screen name="Skin" component={SkinStackScreen} />
      </Drawer.Navigator>
    </NavigationContainer>
  );
};

export default App;
