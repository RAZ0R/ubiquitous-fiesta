import React from 'react';
import { Text, View } from 'react-native';

import Style from '../style/GlobalStyle';

const ItemScreen = () => {
  return (
    <View style={Style.container}>
      <Text style={Style.text}>ItemScreen</Text>
    </View>
  );
};

export default ItemScreen;
