import React from 'react';
import { Text, View } from 'react-native';

import Style from '../style/GlobalStyle';

const SummonerScreen = ({ navigation }) => {
  return (
    <View style={Style.container}>
      <Text style={Style.text}>SummonerScreen</Text>
    </View>
  );
};

export default SummonerScreen;
