import React from 'react';
import { Text, View } from 'react-native';
import Style from '../style/GlobalStyle';

const HomeScreen = () => {
  return (
    <View style={Style.container}>
      <Text style={Style.text}>HomeScreen</Text>
    </View>
  );
};

export default HomeScreen;
