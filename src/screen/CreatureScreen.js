import React from 'react';
import { Text, View } from 'react-native';

import Style from '../style/GlobalStyle';

const CreatureScreen = ({ navigation }) => {
  return (
    <View style={Style.container}>
      <Text style={Style.text}>CreatureScreen</Text>
    </View>
  );
};

export default CreatureScreen;
