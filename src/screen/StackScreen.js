import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import Icon from 'react-native-vector-icons/Ionicons';

import { Colors } from '../style/Variables';

import HomeScreen from '../screen/HomeScreen';
import ItemScreen from '../screen/ItemScreen';
import SummonerScreen from '../screen/SummonerScreen';
import RuneScreen from '../screen/RuneScreen';
import CreatureScreen from '../screen/CreatureScreen';
import SkinScreen from '../screen/SkinScreen';

const HomeStack = createStackNavigator();
const ItemStack = createStackNavigator();
const SummonerStack = createStackNavigator();
const RuneStack = createStackNavigator();
const CreatureStack = createStackNavigator();
const SkinStack = createStackNavigator();

const HomeStackScreen = ({ navigation }) => {
  return (
    <HomeStack.Navigator
      screenOptions={{
        headerStyle: {
          backgroundColor: Colors.stackNavigatorBackgroundColor,
        },
        headerTintColor: Colors.stackNavigatorTextColor,
        headerTitleContainerStyle: {
          fontWeight: 'bold',
        },
      }}
    >
      <HomeStack.Screen
        name="Home"
        component={HomeScreen}
        options={{
          title: 'Home',
          headerLeft: () => (
            <Icon.Button
              name="ios-menu"
              size={25}
              backgroundColor={Colors.sideDrawerColor}
              onPress={() => {
                navigation.openDrawer();
              }}
            />
          ),
        }}
      />
    </HomeStack.Navigator>
  );
};

const ItemStackScreen = ({ navigation }) => {
  return (
    <ItemStack.Navigator
      screenOptions={{
        headerStyle: {
          backgroundColor: Colors.stackNavigatorBackgroundColor,
        },
        headerTintColor: Colors.stackNavigatorTextColor,
        headerTitleContainerStyle: {
          fontWeight: 'bold',
        },
      }}
    >
      <ItemStack.Screen
        name="Items"
        component={ItemScreen}
        options={{
          title: 'Item',
          headerLeft: () => (
            <Icon.Button
              name="ios-menu"
              size={25}
              backgroundColor={Colors.sideDrawerColor}
              onPress={() => {
                navigation.openDrawer();
              }}
            />
          ),
        }}
      />
    </ItemStack.Navigator>
  );
};

const SummonerStackScreen = ({ navigation }) => {
  return (
    <SummonerStack.Navigator
      screenOptions={{
        headerStyle: {
          backgroundColor: Colors.stackNavigatorBackgroundColor,
        },
        headerTintColor: Colors.stackNavigatorTextColor,
        headerTitleContainerStyle: {
          fontWeight: 'bold',
        },
      }}
    >
      <SummonerStack.Screen
        name="Items"
        component={SummonerScreen}
        options={{
          title: 'Summoner',
          headerLeft: () => (
            <Icon.Button
              name="ios-menu"
              size={25}
              backgroundColor={Colors.sideDrawerColor}
              onPress={() => {
                navigation.openDrawer();
              }}
            />
          ),
        }}
      />
    </SummonerStack.Navigator>
  );
};

const RuneStackScreen = ({ navigation }) => {
  return (
    <RuneStack.Navigator
      screenOptions={{
        headerStyle: {
          backgroundColor: Colors.stackNavigatorBackgroundColor,
        },
        headerTintColor: Colors.stackNavigatorTextColor,
        headerTitleContainerStyle: {
          fontWeight: 'bold',
        },
      }}
    >
      <RuneStack.Screen
        name="Items"
        component={RuneScreen}
        options={{
          title: 'Rune',
          headerLeft: () => (
            <Icon.Button
              name="ios-menu"
              size={25}
              backgroundColor={Colors.sideDrawerColor}
              onPress={() => {
                navigation.openDrawer();
              }}
            />
          ),
        }}
      />
    </RuneStack.Navigator>
  );
};

const CreatureStackScreen = ({ navigation }) => {
  return (
    <CreatureStack.Navigator
      screenOptions={{
        headerStyle: {
          backgroundColor: Colors.stackNavigatorBackgroundColor,
        },
        headerTintColor: Colors.stackNavigatorTextColor,
        headerTitleContainerStyle: {
          fontWeight: 'bold',
        },
      }}
    >
      <CreatureStack.Screen
        name="Items"
        component={CreatureScreen}
        options={{
          title: 'Creature',
          headerLeft: () => (
            <Icon.Button
              name="ios-menu"
              size={25}
              backgroundColor={Colors.sideDrawerColor}
              onPress={() => {
                navigation.openDrawer();
              }}
            />
          ),
        }}
      />
    </CreatureStack.Navigator>
  );
};

const SkinStackScreen = ({ navigation }) => {
  return (
    <SkinStack.Navigator
      screenOptions={{
        headerStyle: {
          backgroundColor: Colors.stackNavigatorBackgroundColor,
        },
        headerTintColor: Colors.stackNavigatorTextColor,
        headerTitleContainerStyle: {
          fontWeight: 'bold',
        },
      }}
    >
      <SkinStack.Screen
        name="Items"
        component={SkinScreen}
        options={{
          title: 'Skin',
          headerLeft: () => (
            <Icon.Button
              name="ios-menu"
              size={25}
              backgroundColor={Colors.sideDrawerColor}
              onPress={() => {
                navigation.openDrawer();
              }}
            />
          ),
        }}
      />
    </SkinStack.Navigator>
  );
};

export {
  HomeStackScreen,
  ItemStackScreen,
  SummonerStackScreen,
  RuneStackScreen,
  CreatureStackScreen,
  SkinStackScreen,
};
