import React from 'react';
import { Text, View } from 'react-native';

import Style from '../style/GlobalStyle';

const RuneScreen = ({ navigation }) => {
  return (
    <View style={Style.container}>
      <Text style={Style.text}>RuneScreen</Text>
    </View>
  );
};

export default RuneScreen;
