import React from 'react';
import { View, Text, Image } from 'react-native';
import { DrawerContentScrollView, DrawerItem } from '@react-navigation/drawer';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

import DrawerOptions from '../component/DrawerOptions';

import DrawerContentStyle from '../style/DrawerContentStyle';

const DrawerContent = (props) => {
  return (
    <View style={DrawerContentStyle.container}>
      <DrawerContentScrollView {...props}>
        <View style={DrawerContentStyle.header}>
          <Image
            style={DrawerContentStyle.headerimage}
            source={require('../asset/appIcon.jpg')}
          />
          <View styles={DrawerContentStyle.headertextcontainer}>
            <Text style={DrawerContentStyle.headertext1}>WILD RIFT</Text>
            <Text style={DrawerContentStyle.headertext2}>Guide</Text>
          </View>
        </View>
        <View style={DrawerContentStyle.itemcontainer}>
          <DrawerOptions
            isIcon={true}
            name={'shield-half-full'}
            label={'Build'}
            navigation={props.navigation}
            navigateto={'Build'}
          />
          <DrawerOptions
            isIcon={true}
            name={'sword-cross'}
            label={'Item'}
            navigation={props.navigation}
            navigateto={'Item'}
          />
          <DrawerOptions
            isIcon={true}
            name={'fire'}
            label={'Summoner'}
            navigation={props.navigation}
            navigateto={'Summoner'}
          />
          <DrawerOptions
            isIcon={false}
            name={'scroll'}
            label={'Rune'}
            navigation={props.navigation}
            navigateto={'Rune'}
          />
          <DrawerOptions
            isIcon={true}
            name={'alien-outline'}
            label={'Creature'}
            navigation={props.navigation}
            navigateto={'Creature'}
          />
          <DrawerOptions
            isIcon={true}
            name={'tshirt-crew-outline'}
            label={'Skins'}
            navigation={props.navigation}
            navigateto={'Skin'}
          />
        </View>
      </DrawerContentScrollView>
      <View style={DrawerContentStyle.footercontainer}>
        <DrawerItem
          icon={({ color, size }) => (
            <Icon name="share" color={color} size={size} />
          )}
          label="Share the app"
          onPress={() => {}}
        />
      </View>
    </View>
  );
};

export default DrawerContent;
