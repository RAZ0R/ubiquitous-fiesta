import { StyleSheet } from 'react-native';
import { Colors } from '../style/Variables';

const DrawerContentStyle = StyleSheet.create({
  container: {
    flex: 1,
  },
  header: {
    flexDirection: 'row',
    alignItems: 'center',
    margin: 20,
  },
  headerimage: {
    width: 45,
    height: 45,
    resizeMode: 'contain',
  },
  headertextcontainer: {
    flexDirection: 'column',
  },
  headertext1: {
    paddingLeft: 10,
    fontWeight: 'bold',
  },
  headertext2: {
    paddingLeft: 10,
  },
  itemcontainer: {
    borderColor: Colors.Bordercolor,
    paddingTop: 5,
    borderTopWidth: 1,
    borderBottomWidth: 1,
  },
  footercontainer: { borderColor: Colors.Bordercolor, borderTopWidth: 1 },
});

export default DrawerContentStyle;
