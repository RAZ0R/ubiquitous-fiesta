export const Colors = {
  borderColor: '#A9A9A9',
  stackNavigatorBackgroundColor: '#009387',
  sideDrawerColor: '#009387',
  stackNavigatorTextColor: '#fff',
  globalTextColor: 'green',
};

export const Texts = {
  globalText: 30,
};
