import { StyleSheet } from 'react-native';
import { Colors, Texts } from './Variables';

const Style = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  text: {
    color: Colors.globalTextColor,
    fontSize: Texts.globalText,
  },
  drawerstyle: {
    width: 200,
  },
});

export default Style;
