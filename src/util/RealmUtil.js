/**
 * https://gitlab.com/RAZ0R/ubiquitous-fiesta/-/wikis/development/realm
 */

import Realm from 'realm';

export default class RealmUtil {
  constructor(realm) {
    this.realm = realm;
  }

  static async build(models) {
    const realm = await Realm.open({
      schema: models,
      deleteRealmIfMigrationNeeded: true,
    });
    return new RealmUtil(realm);
  }

  static buildComponentHook(realmUtil, setRealmUtil, models, errCallback) {
    let closeCb = () => {
      if (realmUtil) {
        realmUtil.close();
      }
    };
    let openCb = () => {
      if (!realmUtil || realmUtil.isClosed) {
        RealmUtil.build(models).then(setRealmUtil).catch(errCallback);
      }
    };
    return [
      () => {
        openCb();
        return closeCb;
      },
      [realmUtil],
    ];
  }

  static async do(models, doCallBack) {
    const realmUtil = await RealmUtil.build(models);
    return await doCallBack(realmUtil);
  }

  async getData(model, filtered = '') {
    let data = null;
    if (filtered) {
      data = await this.realm.objects(model.name).filtered(filtered);
    } else {
      data = await this.realm.objects(model.name);
    }
    return data;
  }

  get isClosed() {
    return this.realm == null || this.realm.isClosed;
  }

  close() {
    if (!this.isClosed) {
      this.realm.close();
    }
  }
}
