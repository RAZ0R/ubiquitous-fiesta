import CacheVersion from '../model/CacheVersion';
import { version as currentVersion } from '../../package.json';
import log from './Logger';
import Model from '../constant/Model';
import RealmUtil from './RealmUtil';
import { getCurrentUTCTime } from './TimeUtil';
import CONFIG from '../config';

async function _isCacheVerified() {
  let isVerified = false;
  await RealmUtil.do([CacheVersion], async (realmUtil) => {
    const cacheVersion = await realmUtil.realm
      .objects(CacheVersion.name)
      .sorted('updatedAt', true)[0];
    isVerified = cacheVersion && cacheVersion.version === currentVersion;
  });
  log.info('Verifying Cache!');
  return isVerified;
}

async function updateCacheVersion() {
  await RealmUtil.do([CacheVersion], async (realmUtil) => {
    realmUtil.realm.write(() => {
      realmUtil.realm.create(CacheVersion.name, {
        version: currentVersion,
        updatedAt: getCurrentUTCTime(),
      });
    });
  });
  log.info(`Updated cache version to ${currentVersion}`);
}

async function deleteCache() {
  await RealmUtil.do(Model.ALL, async (realmUtil) => {
    realmUtil.realm.write(() => {
      realmUtil.realm.deleteAll();
      log.info('Deleted all objects successfully!');
    });
  });
}

function _buildCacheForModel(realmUtil, modelName, modelData) {
  try {
    for (const singleData of modelData) {
      realmUtil.realm.create(modelName, singleData);
    }
    log.info(`Cache built successfully for ${modelName}`);
  } catch (error) {
    log.error(`Failed to build cache for ${modelName}`);
    return error;
  }
}

async function buildCache(force = false) {
  if (!force && !CONFIG.alwaysRebuildCache && (await _isCacheVerified())) {
    log.info('Cache verified! No need to rebuild.');
    return;
  } else {
    await deleteCache();
  }
  const { DATA } = require('../constant/Data');
  let errors = [];
  const realmUtil = await RealmUtil.build(Model.ALL);
  try {
    realmUtil.realm.write(() => {
      log.info('Starting transaction for building cache.');
      for (const [modelName, modelData] of Object.entries(DATA)) {
        const error = _buildCacheForModel(realmUtil, modelName, modelData);
        if (error) {
          errors.push(error);
        }
      }
    });
  } finally {
    realmUtil.close();
  }
  if (errors.length) {
    throw new Error(JSON.stringify(errors));
  } else {
    await updateCacheVersion(realmUtil);
  }
}

export default buildCache;
