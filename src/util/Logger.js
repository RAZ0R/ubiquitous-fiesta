import { logger } from 'react-native-logs';
import { ansiColorConsoleSync } from 'react-native-logs/dist/transports/ansiColorConsoleSync';

const config = {
  transport: ansiColorConsoleSync,
};

const log = logger.createLogger(config);

log.setSeverity(__DEV__ ? 'debug' : 'info');

export default log;
