import moment from 'moment';

export const parseTimeFromInt = (timestamp) => {
  return moment.utc(timestamp);
};

export const getCurrentUTCTime = () => {
  return parseInt(moment.utc().format('x'), 10);
};
