const Patch = {
  name: 'Patch',
  properties: {
    version: 'string',
    notes: 'string',
    isLatest: 'bool',
    releaseDate: 'date',
  },
};

export default Patch;
