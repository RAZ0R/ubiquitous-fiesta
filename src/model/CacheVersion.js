const CacheVersion = {
  name: 'CacheVersion',
  properties: {
    version: 'string',
    updatedAt: 'int',
  },
};

export default CacheVersion;
