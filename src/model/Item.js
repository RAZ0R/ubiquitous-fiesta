const Item = {
  name: 'Item',
  properties: {
    category: 'ItemCategory',
    name: 'string',
    description: 'string',
    photo: 'string',
    cost: 'int',
    tier: 'Tier',
  },
};

export default Item;
