const ItemCategory = {
  name: 'ItemCategory',
  properties: {
    name: 'string',
  },
};

export default ItemCategory;
