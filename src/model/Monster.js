const Monster = {
  name: 'Monster',
  properties: {
    name: 'string',
    description: 'string',
    photo: 'string',
    health: 'int',
    locationPhoto: 'string',
    initialTime: 'float',
    respondTime: 'float',
    bounty: 'string',
  },
};

export default Monster;
