const Cost = {
  name: 'Cost',
  properties: {
    unit: 'string',
    amount: 'int',
  },
};

export default Cost;
