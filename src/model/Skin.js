const Skin = {
  name: 'Skin',
  properties: {
    name: 'string',
    photo: 'string',
    isDefault: { type: 'bool', default: false },
  },
};

export default Skin;
