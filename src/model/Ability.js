const Ability = {
  name: 'Ability',
  properties: {
    name: 'string',
    description: 'string',
    isPassive: 'bool',
    range: 'int',
    cost: 'Cost',
    cooldown: 'float',
  },
};

export default Ability;
