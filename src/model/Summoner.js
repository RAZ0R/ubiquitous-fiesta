const Summoner = {
  name: 'Summoner',
  properties: {
    name: 'string',
    description: 'string',
    photo: 'string',
    cooldown: 'float',
  },
};

export default Summoner;
