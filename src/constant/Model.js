/**
 * This file has all the models in their importable order for realm.
 */

import Ability from '../model/Ability';
import Champion from '../model/Champion';
import ChampionCategory from '../model/ChampionCategory';
import Cost from '../model/Cost';
import Item from '../model/Item';
import ItemCategory from '../model/ItemCategory';
import Monster from '../model/Monster';
import Patch from '../model/Patch';
import Rune from '../model/Rune';
import Skin from '../model/Skin';
import Summoner from '../model/Summoner';
import Tier from '../model/Tier';

const ALL = [
  // Should never include internal usage models like CacheVersion
  Cost,
  Ability,
  ChampionCategory,
  Rune,
  ItemCategory,
  Tier,
  Item,
  Summoner,
  Champion,
  Monster,
  Patch,
  Skin,
];

const lookup = (modelName) => {
  return ALL.filter((_model) => _model.name === modelName)[0];
};

const pushUnique = (array, ...args) => {
  for (let arg of args) {
    if (!array.includes(arg)) {
      array.push(arg);
    }
  }
  return array;
};

const get = (modelName) => {
  let desiredModel = lookup(modelName);
  let desiredModelWithDeps = [];
  Object.keys(desiredModel.properties).forEach((propertyName) => {
    let modelNameToLookup = desiredModel.properties[propertyName];
    if (modelNameToLookup instanceof Object) {
      modelNameToLookup = modelNameToLookup.type;
    }
    modelNameToLookup = modelNameToLookup.split('[]')[0];
    let lookedUpModel = lookup(modelNameToLookup);
    if (lookedUpModel && !desiredModelWithDeps.includes(lookedUpModel)) {
      pushUnique(desiredModelWithDeps, ...get(lookedUpModel.name));
    }
  });
  desiredModelWithDeps.push(desiredModel);
  return desiredModelWithDeps;
};

export default { ALL, get };
