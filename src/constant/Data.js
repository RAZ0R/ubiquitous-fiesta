const DATA = {
  Ability: require('../asset/data/ability.json'),
  ChampionCategory: require('../asset/data/championCategory.json'),
  Champion: require('../asset/data/champion.json'),
  Cost: require('../asset/data/cost.json'),
  ItemCategory: require('../asset/data/itemCategory.json'),
  Item: require('../asset/data/item.json'),
  Monster: require('../asset/data/monster.json'),
  Patch: require('../asset/data/patch.json'),
  Rune: require('../asset/data/rune.json'),
  Skin: require('../asset/data/skin.json'),
  Summoner: require('../asset/data/summoner.json'),
  Tier: require('../asset/data/tier.json'),
};

export { DATA };
