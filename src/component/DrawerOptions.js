import React from 'react';
import { DrawerItem } from '@react-navigation/drawer';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';

const DrawerOptions = (props) => {
  if (props.isIcon) {
    return (
      <DrawerItem
        icon={({ color, size }) => (
          <Icon name={props.name} color={color} size={size} />
        )}
        label={props.label}
        onPress={() => {
          props.navigation.navigate(props.navigateto);
        }}
      />
    );
  }
  return (
    <DrawerItem
      icon={({ color, size }) => (
        <FontAwesome5 name={props.name} color={color} size={size} />
      )}
      label={props.label}
      onPress={() => {
        props.navigation.navigate(props.navigateto);
      }}
    />
  );
};

export default DrawerOptions;
