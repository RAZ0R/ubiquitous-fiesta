let config;

const CONFIG = __DEV__
  ? require('./config.dev.json')
  : require('./config.prod.json');

export default CONFIG;
